import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodohomeComponent } from '../app/todohome/todohome.component';

const routes: Routes = [
  { path: "", component: TodohomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
