import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Status {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-todohome',
  templateUrl: './todohome.component.html',
  styleUrls: ['./todohome.component.css']
})
export class TodohomeComponent implements OnInit {
  displayedColumns: string[] = ['name', 'status'];
  dataSource;
  todo = {
    name: '',
    status: ''
  }
  saveButton;
  updateButton;
  selectedId;
  status: Status[] = [
    { value: 'pending', viewValue: 'pending' },
    { value: 'compleated', viewValue: 'compleated' },
    { value: 'closed', viewValue: 'closed' }
  ];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.saveButton = true;
    this.updateButton = false;
    var url = 'http://localhost:8000/todoapi/getdata';

    this.http.get(url)
      .subscribe(data => {
        this.dataSource = data;
        console.log('this.dataSource.data:', this.dataSource)
      });
  }

  save() {
    console.log('inputdata:', this.todo);
    var saveUrl = 'http://localhost:8000/todoapi/create';

    this.http.post(saveUrl, this.todo).subscribe(data => {
      console.log(' after save:', data);
      this.ngOnInit();
    })
  }

  editData(data) {
    console.log('data edit', data);
    this.updateButton = true;
    this.saveButton = false;
    this.todo = data;
    this.selectedId = data._id;
  }

  update() {
    var updateUrl = 'http://localhost:8000/todoapi/update/'+ this.selectedId;
    var updateObject = {
      name: '',
      status: ''
    }
    updateObject.name = this.todo.name;
    updateObject.status = this.todo.status
    console.log('this.todo in update:', updateObject);

    this.http.put(updateUrl, updateObject).subscribe(data => {
      console.log('updated values:', data);
      this.updateButton = false;
      this.saveButton = true;
    })
  }
  deleteData(data) {
    console.log('deletedata:', data)
    this.updateButton = false;
    this.saveButton = false;
    var deleteUrl = 'http://localhost:8000/todoapi/delete/';
    this.http.delete(deleteUrl + data._id)
      .subscribe(data => {
        console.log('this.dataSource.data:', data);
        this.saveButton = true;
        this.ngOnInit();
      })
  }

}
