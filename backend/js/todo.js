const Todoapi = require('../model/todo.model');

exports.create = function (req, res) {
    var createInfor = new Todoapi;
    createInfor.name = req.body.name;
    createInfor.status = req.body.status;
    createInfor.save(function (err, docs) {
        if (!err)
            res.send({ doc: docs });
        else
            res.send(err);
    });
}

exports.getdata = function (req, res) {
    Todoapi.find(function (err, docs) {
        if (err) {
            res.send(err);
        }
        res.send(docs);
    });
}

exports.updatebyid = function (req, res){
    Todoapi.findById(req.params.id ,function(err, docs){
        if(err){
            res.send(err);
        } 
        console.log('req params:', req.params);
        console.log('req body:', req.body);
        docs.name = req.body.name;
        docs.status = req.body.status;
        docs.save(function(err, updatedDoc){
            console.log('updatedDoc:', updatedDoc)
            res.send({docs: updatedDoc})
        });
    })
}

exports.deletebyid = function(req, res){
    Todoapi.remove({_id: req.params.id}, function(err, docs){
        if(err)
        res.send(err)
        res.send({msg: "Record deleted from the data base"})
    })
}

exports.getbyid = function(req, res){
    Todoapi.findById(req.params.id, function(err, docs){
        if(err)
        res.send(err)
        else
        res.send({docs: docs});
    })
}