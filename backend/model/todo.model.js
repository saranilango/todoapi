const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const todoSchema = new Schema({
    name : String,
    status: String
});

module.exports = mongoose.model('TodoCurd', todoSchema);
