const express = require('express');
const router = express.Router();

var todo = require('../js/todo');

/* api*/

router.post('/create', todo.create);
router.get('/getdata', todo.getdata);
router.put('/update/:id', todo.updatebyid);
router.delete('/delete/:id',todo.deletebyid);
router.get('/getbydid/:id', todo.getbyid);

module.exports = router;